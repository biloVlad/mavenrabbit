/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rabbitmongo;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author StoneInside
 */
public class Controller {

    public static void main(String[] args) throws IOException, TimeoutException {
        SetProps props = SetProps.getInstance();
        Client c1 = null;
        try {
            c1 = new Client();
            c1.send("test.info", "Hello");
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (c1 != null) {
                try {
                    c1.close();
                } catch (IOException _ignore) {
                }
            }
        }
    }
}
