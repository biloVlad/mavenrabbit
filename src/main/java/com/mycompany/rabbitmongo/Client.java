package com.mycompany.rabbitmongo;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Client {

    private final Connection connection;
    private final Channel channel;
    private final String replyQueueName;
    private static Logger log;

    public Client() throws IOException, TimeoutException {        
        log = LoggerFactory.getLogger(Client.class);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getProperty("host"));

        connection = factory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(System.getProperty("exchange"), BuiltinExchangeType.TOPIC);

        replyQueueName = channel.queueDeclare().getQueue();
    }

    public String call(String routingKey, String message) throws IOException, InterruptedException {
        final String _correlationId = UUID.randomUUID().toString();

        AMQP.BasicProperties props = new AMQP.BasicProperties.Builder()
                .correlationId(_correlationId)
                .replyTo(replyQueueName)
                .build();

        channel.basicPublish(System.getProperty("exchange"), routingKey, props, message.getBytes("UTF-8"));
        log.info("Basic publish to channel: " + System.getProperty("exchange") + routingKey + props + message.getBytes("UTF-8"));

        final BlockingQueue<String> response = new ArrayBlockingQueue<String>(1);
        channel.basicConsume(replyQueueName, true, new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                if (properties.getCorrelationId().equals(_correlationId)) {
                    response.offer(new String(body, "UTF-8"));
                }
            }
        });

        return response.take();
    }

    public void close() throws IOException {
        connection.close();
    }

    public static void send(String... argv) {
        Client client = null;
        String response = null;
        try {
            client = new Client();

            String routingKey = getRouting(argv);
            log.info("Getting routing key: " + routingKey);
            String message = getMessage(argv);
            log.info("Getting message: " + message);

            System.out.println(" [x] Send '" + routingKey + ": " + message + "'");
            log.info("Send '" + routingKey + ": " + message + "'");

            response = client.call(routingKey, message);
            System.out.println(" [.] Got '" + response + "'");
            log.info("Response: " + response);
        } catch (IOException | TimeoutException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (client != null) {
                try {
                    client.close();
                } catch (IOException _ignore) {
                }
            }
        }
    }

    private static String getRouting(String[] strings) {
        if (strings.length < 1) {
            return "anonymous.info";
        }
        return strings[0];
    }

    private static String getMessage(String[] strings) {
        if (strings.length < 2) {
            log.info("Length of strings < 2");
            return "Hello World!";
        }
        return joinStrings(strings, " ", 1);
    }

    private static String joinStrings(String[] strings, String delimiter, int startIndex) {
        int length = strings.length;
        if (length == 0) {
            return "";
        }
        if (length < startIndex) {
            return "";
        }
        StringBuilder words = new StringBuilder(strings[startIndex]);
        for (int i = startIndex + 1; i < length; i++) {
            words.append(delimiter).append(strings[i]);
        }
        return words.toString();
    }
};
