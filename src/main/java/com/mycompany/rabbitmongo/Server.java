package com.mycompany.rabbitmongo;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeoutException;
import javax.xml.ws.spi.http.HttpExchange;
import org.bson.Document;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.server.ServerConfiguration;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server {

    public static void start(String ... argv) {
        Logger logger = LoggerFactory.getLogger(Server.class);
        logger.info("I'm your info logger");
        logger.debug("I'm your debug logger");
        HttpServer server = HttpServer.createSimpleServer();
        logger.info(server.getListeners().toString());
        ServerConfiguration config = server.getServerConfiguration();

        config.addHttpHandler(
                new org.glassfish.grizzly.http.server.HttpHandler() {
            @Override
            public void service(Request request, Response response) throws Exception {
                final SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
                final String date = format.format(new Date(System.currentTimeMillis()));
                response.setContentType("text/plain");
                response.setContentLength(date.length());
                response.getWriter().write(date);
            }

            //@Override
            public void handle(HttpExchange exchange) throws IOException {
                throw new UnsupportedOperationException("Not supported yet.");
            }
        },
                "/time");

        config.addHttpHandler(
                new org.glassfish.grizzly.http.server.HttpHandler() {
            @Override
            public void service(Request request, Response response) throws Exception {
                String info = "Server information:\n";
                info += "Server name - " + config.getHttpServerName() + "\n"
                        + "Server version - " + config.getHttpServerVersion() + "\n"
                        + "Name - " + config.getName() + "\n"
                        + "Scheme - " + config.getScheme();
                response.setContentType("text/plain");
                response.setContentLength(info.length());
                response.getWriter().write(info);
            }

            //@Override
            public void handle(HttpExchange exchange) throws IOException {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        },
                "/info");
        try {
            server.start();
            System.out.println("Grizzly HTTP server is running...");
        } catch (Exception e) {
            System.err.println(e);
        }

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getProperty("host"));
        logger.info("Connection set host: " + System.getProperty("host"));

        MongoClient mongoClient = new MongoClient();
        MongoDatabase database = mongoClient.getDatabase(System.getProperty("database"));
        logger.info("Database set to: " + System.getProperty("database"));

        Connection connection = null;
        try {
            connection = factory.newConnection();
            final Channel channel = connection.createChannel();

            channel.exchangeDeclare(System.getProperty("exchange"), BuiltinExchangeType.TOPIC);
            logger.info("Exchange declare to " + System.getProperty("exchange") + " of " + BuiltinExchangeType.TOPIC + " type");

            String queueName = channel.queueDeclare().getQueue();
            logger.info("Set queue name to " + queueName);

            if (argv.length < 1) {
                System.err.println("Usage: ReceiveLogsTopic [binding_key]...");
                System.exit(1);
            }

            for (String bindingKey : argv) {
                channel.queueBind(queueName, System.getProperty("exchange"), bindingKey);
                logger.info("Queue " + queueName + " bind to exchange " + System.getProperty("exchange") + " with binding key " + bindingKey);
            }

            //channel.queueDeclare(queueName, false, false, false, null);
            int basicQos = 1;
            channel.basicQos(basicQos);
            logger.info("Set basic Qos of channel of " + basicQos);

            System.out.println(" [x] Awaiting RPC requests");

            Consumer consumer = new DefaultConsumer(channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    AMQP.BasicProperties replyProps = new AMQP.BasicProperties.Builder()
                            .correlationId(properties.getCorrelationId())
                            .build();

                    String response = "";

                    try {
                        String message = new String(body, "UTF-8");
                        System.out.println(" [x] Received '" + envelope.getRoutingKey() + "':'" + message + "'");
                        logger.info("Received '" + envelope.getRoutingKey() + "':'" + message + "'");

                        Document doc = new Document("routingKey", envelope.getRoutingKey())
                                .append("value", message);
                        logger.info("Document generated: " + doc);

                        MongoCollection<Document> collection = database.getCollection(System.getProperty("collection"));
                        logger.info("Get collection database: " + System.getProperty("collection"));

                        collection.insertOne(doc);
                        logger.info("Document '" + doc + "' insert to collection '" + collection + "'");

                        response = " [x] Data '" + envelope.getRoutingKey() + "':'" + message + "' successful inserted in " + System.getProperty("database") + "." + System.getProperty("collection");
                        logger.info("Data '" + envelope.getRoutingKey() + "':'" + message + "' successful inserted in " + System.getProperty("database") + "." + System.getProperty("collection"));

                    } catch (RuntimeException e) {
                        System.out.println(" [.] " + e.toString());
                    } finally {

                        channel.basicPublish("", properties.getReplyTo(), replyProps, response.getBytes("UTF-8"));
                        logger.info("Basic public to channel: " + properties.getReplyTo() + replyProps + response.getBytes("UTF-8"));

                        channel.basicAck(envelope.getDeliveryTag(), false);
                        // RabbitMq consumer worker thread notifies the RPC server owner thread 
                        synchronized (this) {
                            this.notify();
                        }
                    }
                }
            };

            channel.basicConsume(queueName, false, consumer);
            // Wait and be prepared to consume the message from RPC client.
            while (true) {
                synchronized (consumer) {
                    try {
                        consumer.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (IOException _ignore) {
                }
            }
        }
    }

};
