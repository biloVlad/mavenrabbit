/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rabbitmongo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author StoneInside
 */
public class StartServer {
    public static void main(String[] args) {
        SetProps props = SetProps.getInstance(); 
        Server s = new Server();
        s.start("test.*");        
        Logger logger = LoggerFactory.getLogger(StartServer.class);
      logger.info("'sup? I'm your info logger");
      logger.debug("hey HEY hey! I'm your debug logger");
    }
}
