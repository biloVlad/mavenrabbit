package com.mycompany.rabbitmongo;



import java.io.*;
import java.util.Properties;

public class SetProps {

    private static volatile SetProps instance = null;

    private SetProps() {
        try {
            InputStream file = new FileInputStream(new File("config.properties"));
            Properties props = new Properties();
            props.load(file);
            for (String propName : props.stringPropertyNames()) {
                System.setProperty(propName, props.getProperty(propName));
            }
        } catch (Exception e) {
            System.err.println("error" + e);
        }
    }

    public static SetProps getInstance() {
        SetProps localInstance = instance;
        if (localInstance == null) {
            synchronized (SetProps.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SetProps();
                }
            }
        }
        return localInstance;
    }
}
